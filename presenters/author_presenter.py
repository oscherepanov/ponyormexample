from presenters.object_presenter import ObjectPresenter
from presenters.person_presenter import PersonPresenter
from service.author_service import AuthorService
from view.contracts import IAuthorView


class AuthorPresenter(PersonPresenter):
    def __init__(self, view: IAuthorView) -> None:
        ObjectPresenter.__init__(self, view, AuthorService)