from presenters.object_presenter import ObjectPresenter
from service.action_service import ActionService
from service.book_service import BookService
from service.employee_service import EmployeeService
from service.event_log_service import EventLogService
from service.reader_service import ReaderService
from view.contracts import IEventLogView


class EventLogPresenter(ObjectPresenter):
    def __init__(self, view: IEventLogView) -> None:
        ObjectPresenter.__init__(self, view, EventLogService)
        self.books = {i.name: i for i in BookService().get_all()}
        self.readers = {i.full_name(): i for i in ReaderService().get_all()}
        self.employees = {i.full_name(): i for i in EmployeeService().get_all()}
        self.actions = {i.name: i for i in ActionService().get_all()}

        self.view.set_book_items(self.books.keys())
        self.view.set_reader_items(self.readers.keys())
        self.view.set_employee_items(self.employees.keys())
        self.view.set_action_items(self.actions.keys())

    def to_view(self):
        self.service.load_book(self.object)
        if self.object.book:
            self.view.set_book(self.object.book.name)

        self.service.load_reader(self.object)
        if self.object.reader:
            self.view.set_reader(self.object.reader.full_name())

        self.service.load_employee(self.object)
        if self.object.employee:
            self.view.set_employee(self.object.employee.full_name())

        self.service.load_action(self.object)
        if self.object.action:
            self.view.set_action(self.object.action.name)

        self.view.set_created_at(self.object.created_at)

    def from_view(self):
        self.object.book = self.books[self.view.get_book()]
        self.object.reader = self.readers[self.view.get_reader()]
        self.object.employee = self.employees[self.view.get_employee()]
        self.object.action = self.actions[self.view.get_action()]
        self.object.created_at = self.view.get_created_at()

