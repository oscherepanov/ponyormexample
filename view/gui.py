import sys
from binding import *

from PyQt5.QtWidgets import QApplication

from registry.registry import Registry
from view.q_main import QMain

app = QApplication(sys.argv)

window = QMain()
Registry.get_instance().set_main_presenter(window.get_presenter())
window.show()
app.exec_()