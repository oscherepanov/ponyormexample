from entites.reader import Reader
from repository.repository_imp import CRUDRepositoryImp


class ReaderRepository(CRUDRepositoryImp):
    def __init__(self):
        super().__init__(Reader)

    @staticmethod
    def to_dict(reader):
        return reader.to_dict()

    @staticmethod
    def to_dict(reader):
        d = reader.to_dict()
        del d['classtype']
        return d