from dto.author_dto import AuthorDTO
from repository.author_repository import AuthorRepository
from service.service import CRUDService


class AuthorService(CRUDService):
    def __init__(self):
        super().__init__(AuthorDTO, AuthorRepository())