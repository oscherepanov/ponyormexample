from PyQt5.QtWidgets import QHeaderView, QWidget

from presenters.books_presenter import BooksPresenter
from view.q_objects_view import QObjectsView


class QBooksView(QObjectsView):
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        QObjectsView.__init__(self, parent)
        self.set_presenter(BooksPresenter(view=self))

    def init_table(self):
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(['Авторы', 'Названия'])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)