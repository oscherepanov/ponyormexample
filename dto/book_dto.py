from datetime import datetime

from dto.author_dto import AuthorDTO
from dto.dto import DTO
from dto.genre_dto import GenreDTO


class BookDTO(DTO):

    classes = {'authors': AuthorDTO,
               'genres': GenreDTO}

    def __init__(self, name=None, authors=None, genres=None, year=None, id=None):
        self.id = id
        self.name = name
        self.authors = authors
        self.genres = genres
        if not year:
            self.year = datetime.today().year
        else:
            self.year = year

    def _str_authors(self):
        if self.authors:
            return ", ".join(map(lambda item: str(item.full_name()), self.authors))
        else:
            return ""

    def _str_genres(self):
        if self.genres:
            return ", ".join(map(lambda item: str(item.name), self.genres))
        return ""

    def __str__(self):
        return f'BookDTO -> id: {self.id}, name: {self.name}, authors: {self._str_authors()}, ' \
               f'genres: {self._str_genres()}, year: {self.year}'

    @staticmethod
    def class_by_name(name):
        return BookDTO.classes[name]