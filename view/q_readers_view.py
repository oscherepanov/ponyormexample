from PyQt5.QtWidgets import QHeaderView, QWidget

from presenters.readers_presenter import ReadersPresenter
from view.q_objects_view import QObjectsView


class QReadersView(QObjectsView):
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        QObjectsView.__init__(self, parent)
        self.set_presenter(ReadersPresenter(view=self))

    def init_table(self):
        self.table.setColumnCount(1)
        self.table.setHorizontalHeaderLabels(['ФИО'])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)