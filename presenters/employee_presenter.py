from presenters.object_presenter import ObjectPresenter
from service.employee_service import EmployeeService
from service.position_service import PositionService
from view.contracts import IEmployeeView


class EmployeePresenter(ObjectPresenter):
    def __init__(self, view: IEmployeeView) -> None:
        ObjectPresenter.__init__(self, view, EmployeeService)
        self.positions = {i.name: i for i in PositionService().get_all()}
        self.view.set_position_items(self.positions.keys())

    def to_view(self):
        self.view.set_first_name(self.object.first_name)
        self.view.set_last_name(self.object.last_name)
        self.view.set_patronymic(self.object.patronymic)
        if self.object.position:
            self.view.set_position(self.object.position.name)

    def from_view(self):
        self.object.first_name = self.view.get_first_name()
        self.object.last_name = self.view.get_last_name()
        self.object.patronymic = self.view.get_patronymic()
        self.object.position = self.positions[self.view.get_position()]

