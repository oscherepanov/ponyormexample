from PyQt5.QtWidgets import QHeaderView, QWidget

from presenters.event_logs_presenter import EventLogsPresenter
from view.q_objects_view import QObjectsView


class QEventLogsView(QObjectsView):
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        QObjectsView.__init__(self, parent)
        self.set_presenter(EventLogsPresenter(view=self))

    def init_table(self):
        self.table.setColumnCount(4)
        self.table.setHorizontalHeaderLabels(['Читатель', 'Книга', 'Действие', 'Дата'])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)