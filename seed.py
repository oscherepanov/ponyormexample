from pony.orm import db_session
from binding import *

from entites.action import Action
from entites.genre import Genre

try:
    with db_session:
        Action(name='Выдача')  #id = 1
        Action(name='Возврат') #id = 2

        Genre(name='Роман') #id = 1
        Genre(name='Поэма') #id = 2
except Exception:
    pass