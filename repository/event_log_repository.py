from pony.orm import db_session

from entites.action import Action
from entites.book import Book
from entites.employee import Employee
from entites.event_log import EventLog
from entites.reader import Reader
from repository.repository_imp import CRUDRepositoryImp


class EventLogRepository(CRUDRepositoryImp):
    def __init__(self):
        super().__init__(EventLog)

    @staticmethod
    @db_session
    def from_dict(event):
        args = event
        args['book'] = Book.get(id=args['book']['id'])
        args['reader'] = Reader.get(id=args['reader']['id'])
        args['employee'] = Employee.get(id=args['employee']['id'])
        args['action'] = Action.get(id=args['action']['id'])
        return args

    @staticmethod
    def to_dict(employee):
        d = employee.to_dict()
        d['book'] = {'id': d['book']}
        d['reader'] = {'id': d['reader']}
        d['employee'] = {'id': d['employee']}
        d['action'] = {'id': d['action']}
        return d