from dto.employee_dto import EmployeeDTO
from dto.position_dto import PositionDTO
from repository.employee_repository import EmployeeRepository
from repository.position_repository import PositionRepository
from service.service import CRUDService


class EmployeeService(CRUDService):
    def __init__(self):
        super().__init__(EmployeeDTO, EmployeeRepository())
        self.position_repository = PositionRepository()

    def load_position(self, emp):
        emp.position = PositionDTO.from_dict(self.position_repository.find(emp.position.id))