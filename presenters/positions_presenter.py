from dto.position_dto import PositionDTO
from presenters.objects_presenter import ObjectsPresenter
from service.position_service import PositionService
from view.contracts import IObjectsView
from view.q_position_view import QPositionView


class PositionsPresenter(ObjectsPresenter):
    def __init__(self, view: IObjectsView) -> None:
        ObjectsPresenter.__init__(self, view, PositionService,  QPositionView, PositionDTO)

    def fill_row(self, row: int, obj: PositionDTO) -> None:
        self.view.set_item(row, 0, obj.name)

