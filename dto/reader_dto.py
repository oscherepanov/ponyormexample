from dto.person_dto import PersonDTO


class ReaderDTO(PersonDTO):
    def __init__(self, first_name=None, last_name=None, patronymic=None, id=None):
        super().__init__(first_name, last_name, patronymic, id)

    def __str__(self):
        return f'ReaderDTO -> id: {self.id}, first_name: {self.first_name}, last_name: {self.last_name}, ' \
               f'patronymic: {self.patronymic}'