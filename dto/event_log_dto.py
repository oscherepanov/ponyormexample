from dto.action_dto import ActionDTO
from dto.book_dto import BookDTO
from dto.dto import DTO
from dto.employee_dto import EmployeeDTO
from dto.reader_dto import ReaderDTO


class EventLogDTO(DTO):

    classes = {'book': BookDTO,
               'reader': ReaderDTO,
               'employee': EmployeeDTO,
               'action': ActionDTO}

    def __init__(self, book=None, reader=None, employee=None, action=None, created_at=None, id=None):
        self.book = book
        self.reader = reader
        self.employee = employee
        self.action = action
        self.created_at = created_at
        self.id = id

    def __str__(self):
        return f'EmployeeDTO -> id: {self.id}, reader: {str(self.reader)}, employee: {str(self.employee)},' \
               f'book: {str(self.book)}, action: {str(self.action)}, created_at: {self.created_at}'

    @staticmethod
    def class_by_name(name):
        return EventLogDTO.classes[name]

