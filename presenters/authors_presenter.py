from dto.author_dto import AuthorDTO
from presenters.objects_presenter import ObjectsPresenter
from service.author_service import AuthorService
from view.contracts import IObjectsView
from view.q_author_view import QAuthorView


class AuthorsPresenter(ObjectsPresenter):
    def __init__(self, view: IObjectsView) -> None:
        ObjectsPresenter.__init__(self, view, AuthorService,  QAuthorView, AuthorDTO)

    def fill_row(self, row: int, obj: AuthorDTO) -> None:
        self.view.set_item(row, 0, obj.full_name())

