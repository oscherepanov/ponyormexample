from PyQt5.QtWidgets import QWidget

from presenters.contracts import IMainPresenter
from view.contracts import IMainView
from view.q_books_view import QBooksView
from view.q_employees_view import QEmployeesView
from view.q_authors_view import QAuthorsView
from view.q_event_logs_view import QEventLogsView
from view.q_positions_view import QPositionsView
from view.q_readers_view import QReadersView


class MainPresenter(IMainPresenter):
    def __init__(self, view: IMainView) -> None:
        self.view = view

    def show_positions(self) -> None:
        self.view.add_central_widget(QPositionsView(self.view))

    def show_authors(self) -> None:
        self.view.add_central_widget(QAuthorsView(self.view))

    def show_readers(self) -> None:
        self.view.add_central_widget(QReadersView(self.view))

    def show_employees(self) -> None:
        self.view.add_central_widget(QEmployeesView(self.view))

    def show_books(self) -> None:
        self.view.add_central_widget(QBooksView(self.view))

    def show_event_logs(self) -> None:
        self.view.add_central_widget(QEventLogsView(self.view))

    def add_widget(self, widget: QWidget) -> None:
        self.view.add_central_widget(widget)

    def remove_widget(self, widget: QWidget) -> None:
        self.view.remove_central_widget(widget)

