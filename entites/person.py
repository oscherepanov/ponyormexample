from pony.orm import composite_key

from database import db, PrimaryKey, Required, Optional


class Person(db.Entity):
    id = PrimaryKey(int, auto=True)
    first_name = Required(str)
    last_name = Required(str)
    patronymic = Optional(str)
    composite_key(first_name, last_name, patronymic)
