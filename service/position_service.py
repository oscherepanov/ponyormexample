from dto.position_dto import PositionDTO
from repository.position_repository import PositionRepository
from service.service import CRUDService


class PositionService(CRUDService):
    def __init__(self):
        super().__init__(PositionDTO, PositionRepository())