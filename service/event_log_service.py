from dto.action_dto import ActionDTO
from dto.book_dto import BookDTO
from dto.employee_dto import EmployeeDTO
from dto.event_log_dto import EventLogDTO
from dto.reader_dto import ReaderDTO
from repository.action_repository import ActionRepository
from repository.book_respository import BookRepository
from repository.employee_repository import EmployeeRepository
from repository.event_log_repository import EventLogRepository
from repository.reader_respository import ReaderRepository
from service.service import CRUDService


class EventLogService(CRUDService):
    def __init__(self):
        super().__init__(EventLogDTO, EventLogRepository())
        self.book_repository = BookRepository()
        self.reader_repository = ReaderRepository()
        self.employee_repository = EmployeeRepository()
        self.action_repository = ActionRepository()

    def load_book(self, event):
        if event.book:
            event.book = BookDTO.from_dict(self.book_repository.find(event.book.id))

    def load_reader(self, event):
        if event.reader:
            event.reader = ReaderDTO.from_dict(self.reader_repository.find(event.reader.id))

    def load_employee(self, event):
        if event.employee:
            event.employee = EmployeeDTO.from_dict(self.employee_repository.find(event.employee.id))

    def load_action(self, event):
        if event.action:
            event.action = ActionDTO.from_dict(self.action_repository.find(event.action.id))