from dto.employee_dto import EmployeeDTO
from presenters.objects_presenter import ObjectsPresenter
from service.employee_service import EmployeeService
from view.contracts import IObjectsView
from view.q_employee_view import QEmployeeView


class EmployeesPresenter(ObjectsPresenter):
    def __init__(self, view: IObjectsView) -> None:
        ObjectsPresenter.__init__(self, view, EmployeeService,  QEmployeeView, EmployeeDTO)

    def fill_row(self, row: int, obj: EmployeeDTO) -> None:
        self.view.set_item(row, 0, obj.full_name())
        self.service.load_position(obj)
        self.view.set_item(row, 1, obj.position.name)


