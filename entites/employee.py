from pony.orm import Required, Set
from entites.person import Person


class Employee(Person):
    position = Required('Position')
    event_logs = Set('EventLog')