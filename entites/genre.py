from database import db, PrimaryKey, Required, Set


class Genre(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str, unique=True)
    books = Set('Book')