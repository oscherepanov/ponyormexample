from dto.dto import DTO


class ActionDTO(DTO):
    def __init__(self, name=None, id=None):
        self.id = id
        self.name = name

    def __str__(self):
        return f"ActionDTO: name={self.name}"