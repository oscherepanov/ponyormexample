from pony.orm import db_session

from entites.action import Action
from repository.repository_imp import RORepositoryImp


class ActionRepository(RORepositoryImp):

    _kind = {'issue': 1, 'ret': 2}

    @staticmethod
    def to_dict(action):
        return action.to_dict()

    def __init__(self):
        super().__init__(Action)

    @db_session
    def issue(self):
        return self.find(ActionRepository._kind['issue'])

    @db_session
    def ret(self):
        return self.find(ActionRepository._kind['ret'])