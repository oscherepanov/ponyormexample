from dto.person_dto import PersonDTO
from dto.position_dto import PositionDTO


class EmployeeDTO(PersonDTO):

    classes = {'position': PositionDTO}

    def __init__(self, first_name=None, last_name=None, patronymic=None, id=None, position=None):
        super().__init__(first_name, last_name, patronymic, id)
        self.position = position

    def __str__(self):
        return f'EmployeeDTO -> id: {self.id}, first_name: {self.first_name}, last_name: {self.last_name},' \
               f'patronymic: {self.patronymic}, ' + str(self.position)

    @staticmethod
    def class_by_name(name):
        return EmployeeDTO.classes[name]
