from dto.book_dto import BookDTO
from presenters.objects_presenter import ObjectsPresenter
from service.book_service import BookService
from view.contracts import IObjectsView
from view.q_book_view import QBookView


class BooksPresenter(ObjectsPresenter):
    def __init__(self, view: IObjectsView) -> None:
        ObjectsPresenter.__init__(self, view, BookService,  QBookView, BookDTO)

    def fill_row(self, row: int, obj: BookDTO) -> None:
        self.service.load_authors(obj)
        self.view.set_item(row, 0, ', '.join(map(lambda item: item.full_name(), obj.authors)))
        self.view.set_item(row, 1, obj.name)

