from presenters.object_presenter import ObjectPresenter
from presenters.person_presenter import PersonPresenter
from service.reader_service import ReaderService
from view.contracts import IAuthorView


class ReaderPresenter(PersonPresenter):
    def __init__(self, view: IAuthorView) -> None:
        ObjectPresenter.__init__(self, view, ReaderService)