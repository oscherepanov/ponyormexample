from pony.orm import db_session, Set

from entites.author import Author
from entites.book import Book
from entites.genre import Genre
from repository.repository_imp import CRUDRepositoryImp


class BookRepository(CRUDRepositoryImp):
    def __init__(self):
        super().__init__(Book)

    @staticmethod
    @db_session
    def from_dict(book):
        args = book
        args['genres'] = set(map(lambda arg: Genre.get(id=arg['id']), args['genres']))
        args['authors'] = set(map(lambda arg: Author.get(id=arg['id']), args['authors']))
        return args

    @staticmethod
    @db_session
    def to_dict(book):
        d = book.to_dict(with_collections=True, exclude='event_logs')
        d['genres'] = list(map(lambda item: {'id': item}, d['genres']))
        d['authors'] = list(map(lambda item: {'id': item}, d['authors']))
        return d

    @db_session
    def all(self, **attrs):
        return self.to_collection_dict(self.klass.select(**attrs).prefetch(Author, Genre)[:])