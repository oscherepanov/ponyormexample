from abc import ABC, abstractmethod

from PyQt5.QtWidgets import QWidget

from presenters.contracts import IObjectPresenter, IObjectsPresenter


class IMainView(ABC):
    @abstractmethod
    def add_central_widget(self, widget: QWidget) -> None:
        pass

    @abstractmethod
    def remove_central_widget(self, widget: QWidget) -> None:
        pass


class IObjectsView(ABC):
    @abstractmethod
    def set_table_row(self, row: int) -> None:
        pass

    @abstractmethod
    def set_item(self, row: int, col: int, value: str) -> None:
        pass

    @abstractmethod
    def get_selected_row_index(self) -> int:
        pass

    @abstractmethod
    def remove_row(self, index: int) -> None:
        pass

    @abstractmethod
    def add_row(self) -> None:
        pass

    @abstractmethod
    def get_presenter(self) -> IObjectsPresenter:
        pass


class IPositionView(ABC):
    @abstractmethod
    def get_name(self) -> str:
        pass

    @abstractmethod
    def set_name(self, value: str) -> None:
        pass

    @abstractmethod
    def get_presenter(self) -> IObjectPresenter:
        pass


class IPersonView(ABC):
    @abstractmethod
    def get_first_name(self) -> str:
        pass

    @abstractmethod
    def set_first_name(self, name: str) -> None:
        pass

    @abstractmethod
    def get_last_name(self) -> str:
        pass

    @abstractmethod
    def set_last_name(self, name: str) -> None:
        pass

    @abstractmethod
    def get_patronymic(self) -> str:
        pass

    @abstractmethod
    def set_patronymic(self, name: str) -> None:
        pass

    @abstractmethod
    def get_presenter(self) -> IObjectPresenter:
        pass


class IAuthorView(IPersonView, ABC):
    pass


class IEmployeeView(IPersonView, ABC):
    @abstractmethod
    def get_position(self) -> str:
        pass

    @abstractmethod
    def set_position(self, index: str) -> None:
        pass

    @abstractmethod
    def set_position_items(self, positions: list) -> None:
        pass


class IBookView(ABC):
    @abstractmethod
    def set_author_items(self, items: str):
        pass

    @abstractmethod
    def set_genre_items(self, items: str):
        pass

    @abstractmethod
    def set_name(self, name):
        pass

    @abstractmethod
    def set_year(self, year):
        pass

    @abstractmethod
    def set_authors(self, authors):
        pass

    @abstractmethod
    def get_authors(self):
        pass

    @abstractmethod
    def set_genres(self, genres):
        pass

    @abstractmethod
    def get_genres(self):
        pass

    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def get_year(self):
        pass


class IEventLogView(ABC):
    @abstractmethod
    def set_book_items(self, items: str):
        pass

    @abstractmethod
    def set_reader_items(self, items: str):
        pass

    @abstractmethod
    def set_employee_items(self, items: str):
        pass

    @abstractmethod
    def set_action_items(self, items: str):
        pass

    @abstractmethod
    def set_book(self, name: str):
        pass

    @abstractmethod
    def get_book(self) -> str:
        pass

    @abstractmethod
    def set_reader(self, name: str):
        pass

    @abstractmethod
    def get_reader(self) -> str:
        pass

    @abstractmethod
    def set_employee(self, name: str):
        pass

    @abstractmethod
    def get_employee(self) -> str:
        pass

    @abstractmethod
    def set_action(self, name: str):
        pass

    @abstractmethod
    def get_action(self):
        pass

    @abstractmethod
    def set_created_at(self, value):
        pass

    @abstractmethod
    def get_created_at(self):
        pass
