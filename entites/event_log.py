from datetime import datetime

from database import db, PrimaryKey, Required


class EventLog(db.Entity):
    id = PrimaryKey(int, auto=True)
    book = Required('Book')
    reader = Required('Reader')
    employee = Required('Employee')
    action = Required('Action')
    created_at = Required(datetime, default=datetime.utcnow())