from dto.dto import DTO


class PersonDTO(DTO):
    def __init__(self, first_name, last_name, patronymic=None, id=None):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.patronymic = patronymic

    def full_name(self):
        return str(self.first_name) + " " + str(self.last_name) + ' ' + str(self.patronymic)
