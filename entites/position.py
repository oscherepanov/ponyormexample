from database import db, PrimaryKey, Required, Set


class Position(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str, unique=True)
    employees = Set('Employee')