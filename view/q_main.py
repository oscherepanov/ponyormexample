from PyQt5.QtWidgets import QMainWindow, qApp, QAction, QStackedWidget, QWidget

from presenters.contracts import IMainPresenter
from presenters.main_presenter import MainPresenter
from view.contracts import IMainView


class QMain(QMainWindow):

    __meta_class__ = IMainView

    def __init__(self) -> None:
        QMainWindow.__init__(self)
        self.stacked_widget = None
        self.menu = None
        self.entities = None
        self.set_ui()
        self.presenter = MainPresenter(self)

    def set_ui(self) -> None:
        self.create_menu()
        self.setWindowTitle("Библиотека")
        geometry = qApp.desktop().availableGeometry(self)
        self.setFixedSize(geometry.width() * 0.8, geometry.height() * 0.8)
        self.stacked_widget = QStackedWidget(self)
        self.setCentralWidget(self.stacked_widget)

    def create_menu(self) -> None:
        self.menu = self.menuBar()
        self.entities = self.menu.addMenu("Сущности")

        position_action = QAction('Должности', self)
        self.entities.addAction(position_action)
        position_action.triggered.connect(self.show_positions)

        author_action = QAction('Авторы', self)
        self.entities.addAction(author_action)
        author_action.triggered.connect(self.show_authors)

        reader_action = QAction('Читатели', self)
        self.entities.addAction(reader_action)
        reader_action.triggered.connect(self.show_readers)

        emp_action = QAction('Сотрудники', self)
        self.entities.addAction(emp_action)
        emp_action.triggered.connect(self.show_employees)

        book_action = QAction('Книги', self)
        self.entities.addAction(book_action)
        book_action.triggered.connect(self.show_books)

        event_logs_action = QAction('Журнал', self)
        self.entities.addAction(event_logs_action)
        event_logs_action.triggered.connect(self.show_event_logs)

    def add_central_widget(self, widget: QWidget) -> None:
        self.stacked_widget.addWidget(widget)
        self.stacked_widget.setCurrentIndex(self.stacked_widget.count() - 1)

    def remove_central_widget(self, widget: QWidget) -> None:
        self.stacked_widget.removeWidget(widget)

    def show_positions(self) -> None:
        self.presenter.show_positions()

    def show_authors(self) -> None:
        self.presenter.show_authors()

    def show_readers(self) -> None:
        self.presenter.show_readers()

    def show_employees(self) -> None:
        self.presenter.show_employees()

    def show_books(self) -> None:
        self.presenter.show_books()

    def show_event_logs(self) -> None:
        self.presenter.show_event_logs()

    def get_presenter(self) -> IMainPresenter:
        return self.presenter

