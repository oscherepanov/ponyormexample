from dto.genre_dto import GenreDTO
from repository.genre_repository import GenreRepository
from service.service import ROService


class GenreService(ROService):
    def __init__(self):
        super().__init__(GenreDTO, GenreRepository())