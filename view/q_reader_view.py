from presenters.reader_presenter import ReaderPresenter
from view.q_person_view import QPersonView


class QReaderView(QPersonView):

    def __init__(self, parent=None):
        QPersonView.__init__(self, parent)
        self.presenter = ReaderPresenter(self)

    def ok(self):
        self.presenter.ok()

    def cancel(self):
        self.presenter.cancel()

    def get_presenter(self):
        return self.presenter
