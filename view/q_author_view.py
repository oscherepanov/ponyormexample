from presenters.author_presenter import AuthorPresenter
from view.q_person_view import QPersonView


class QAuthorView(QPersonView):

    def __init__(self, parent=None):
        QPersonView.__init__(self, parent)
        self.presenter = AuthorPresenter(self)

    def ok(self):
        self.presenter.ok()

    def cancel(self):
        self.presenter.cancel()

    def get_presenter(self):
        return self.presenter
