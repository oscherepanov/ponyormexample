from dto.dto import DTO


class GenreDTO(DTO):
    def __init__(self, name=None, id=None):
        self.name = name
        self.id = id

    def __str__(self):
        return f'GenreDTO: name: {self.name}'
