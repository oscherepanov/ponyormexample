from presenters.object_presenter import ObjectPresenter
from service.position_service import PositionService
from view.contracts import IPositionView


class PositionPresenter(ObjectPresenter):
    def __init__(self, view: IPositionView) -> None:
        ObjectPresenter.__init__(self, view, PositionService)

    def to_view(self):
        self.view.set_name(self.object.name)

    def from_view(self):
        self.object.name = self.view.get_name()
