from database import db, PrimaryKey, Required, Set


class Action(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str, unique=True)
    event_logs = Set('EventLog')