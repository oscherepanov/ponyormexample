from dto.action_dto import ActionDTO
from repository.action_repository import ActionRepository
from service.service import ROService


class ActionService(ROService):
    def __init__(self):
        super().__init__(ActionDTO, ActionRepository())