from dto.reader_dto import ReaderDTO
from presenters.objects_presenter import ObjectsPresenter
from service.reader_service import ReaderService
from view.contracts import IObjectsView
from view.q_reader_view import QReaderView


class ReadersPresenter(ObjectsPresenter):
    def __init__(self, view: IObjectsView) -> None:
        ObjectsPresenter.__init__(self, view, ReaderService,  QReaderView, ReaderDTO)

    def fill_row(self, row: int, obj: ReaderDTO) -> None:
        self.view.set_item(row, 0, obj.full_name())

