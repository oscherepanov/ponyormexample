from presenters.object_presenter import ObjectPresenter
from service.author_service import AuthorService
from service.book_service import BookService
from service.genre_service import GenreService
from view.contracts import IEmployeeView, IBookView


class BookPresenter(ObjectPresenter):
    def __init__(self, view: IBookView) -> None:
        ObjectPresenter.__init__(self, view, BookService)
        self.authors = {i.full_name(): i for i in AuthorService().get_all()}
        self.genres = {i.name: i for i in GenreService().get_all()}
        self.view.set_author_items(self.authors.keys())
        self.view.set_genre_items(self.genres.keys())

    def to_view(self):
        self.view.set_year(self.object.year)
        self.view.set_name(self.object.name)
        if self.object.authors:
            self.view.set_authors([i.full_name() for i in self.object.authors])
        self.service.load_genres(self.object)
        if self.object.genres:
            self.view.set_genres([i.name for i in self.object.genres])

    def from_view(self):
        self.object.year = self.view.get_year()
        self.object.name = self.view.get_name()
        self.object.authors = [self.authors[i] for i in self.view.get_authors()]
        self.object.genres = [self.genres[i] for i in self.view.get_genres()]

