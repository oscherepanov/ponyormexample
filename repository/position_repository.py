from entites.position import Position
from repository.repository_imp import CRUDRepositoryImp


class PositionRepository(CRUDRepositoryImp):
    def __init__(self):
        super().__init__(Position)

    @staticmethod
    def to_dict(position):
        return position.to_dict()
