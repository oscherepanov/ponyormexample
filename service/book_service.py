from dto.author_dto import AuthorDTO
from dto.book_dto import BookDTO
from dto.genre_dto import GenreDTO
from repository.author_repository import AuthorRepository
from repository.book_respository import BookRepository
from repository.genre_repository import GenreRepository
from service.service import CRUDService


class BookService(CRUDService):
    def __init__(self):
        super().__init__(BookDTO, BookRepository())
        self.author_repository = AuthorRepository()
        self.genre_repository = GenreRepository()

    def load_authors(self, book):
        if book.authors:
            book.authors = list(map(lambda item: AuthorDTO.from_dict(self.author_repository.find(item.id)), book.authors))

    def load_genres(self, book):
        if book.genres:
            book.genres = list(map(lambda item: GenreDTO.from_dict(self.genre_repository.find(item.id)), book.genres))
