from PyQt5.QtWidgets import QLabel, QComboBox

from presenters.employee_presenter import EmployeePresenter
from view.q_person_view import QPersonView


class QEmployeeView(QPersonView):

    def __init__(self, parent=None):
        QPersonView.__init__(self, parent)

        position_label = QLabel("Должность", parent=self)
        self.position_cmb = QComboBox(parent=self)
        main_layout = self.layout()
        main_layout.insertWidget(6, position_label)
        main_layout.insertWidget(7, self.position_cmb)

        self.presenter = EmployeePresenter(self)

    def set_position_items(self, items: str):
        self.position_cmb.addItems(items)

    def set_position(self, name):
        self.position_cmb.setCurrentText(name)

    def get_position(self):
        return self.position_cmb.currentText()

    def ok(self):
        self.presenter.ok()

    def cancel(self):
        self.presenter.cancel()

    def get_presenter(self):
        return self.presenter
