from dto.reader_dto import ReaderDTO
from repository.reader_respository import ReaderRepository
from service.service import CRUDService


class ReaderService(CRUDService):
    def __init__(self):
        super().__init__(ReaderDTO, ReaderRepository())