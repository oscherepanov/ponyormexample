from dto.dto import DTO


class PositionDTO(DTO):
    def __init__(self, name=None, id=None):
        self.name = name
        self.id = id

    def __str__(self):
        return f'PositionDTO -> id: {self. id}, name: {self.name}'