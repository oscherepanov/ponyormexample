from dto.event_log_dto import EventLogDTO
from presenters.objects_presenter import ObjectsPresenter
from service.event_log_service import EventLogService
from view.contracts import IObjectsView
from view.q_event_log_view import QEventLogView


class EventLogsPresenter(ObjectsPresenter):
    def __init__(self, view: IObjectsView) -> None:
        ObjectsPresenter.__init__(self, view, EventLogService,  QEventLogView, EventLogDTO)

    def fill_row(self, row: int, obj: EventLogDTO) -> None:
        self.service.load_reader(obj)
        self.service.load_book(obj)
        self.service.load_action(obj)

        self.view.set_item(row, 0, obj.reader.full_name())
        self.view.set_item(row, 1, obj.book.name)
        self.view.set_item(row, 2, obj.action.name)
        self.view.set_item(row, 3, str(obj.created_at))

