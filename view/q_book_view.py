from datetime import datetime

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QWidget, QListWidget, QLineEdit, QSpinBox, QVBoxLayout, QListWidgetItem, \
    QPushButton, QHBoxLayout

from presenters.book_presenter import BookPresenter



class QBookView(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        authors_label = QLabel("Авторы", self)
        self.authors_list = QListWidget(self)

        name_label = QLabel("Название", self)
        self.name_edit = QLineEdit(self)

        year_label = QLabel("Год", self)
        self.year_edit = QSpinBox(self)
        self.year_edit.setMinimum(0)
        self.year_edit.setMaximum(datetime.today().year)

        genres_label = QLabel("Жанры", self)
        self.genres_list = QListWidget(self)

        layout = QVBoxLayout()
        layout.addWidget(authors_label)
        layout.addWidget(self.authors_list)
        layout.addWidget(name_label)
        layout.addWidget(self.name_edit)
        layout.addWidget(year_label)
        layout.addWidget(self.year_edit)
        layout.addWidget(genres_label)
        layout.addWidget(self.genres_list)

        self.ok_btn = QPushButton('OK', parent=self)
        self.ok_btn.clicked.connect(self.ok)
        self.ok_cancel = QPushButton('Cancel', parent=self)
        self.ok_cancel.clicked.connect(self.cancel)

        btn_layout = QHBoxLayout()
        btn_layout.addWidget(self.ok_btn)
        btn_layout.addWidget(self.ok_cancel)

        layout.addLayout(btn_layout)
        layout.addStretch(0)

        self.setLayout(layout)

        self.presenter = BookPresenter(self)

    def set_author_items(self, items: str):
        for i in items:
            item = QListWidgetItem(i)
            item.setCheckState(Qt.Unchecked)
            self.authors_list.addItem(item)

    def set_genre_items(self, items: str):
        for i in items:
            item = QListWidgetItem(i)
            item.setCheckState(Qt.Unchecked)
            self.genres_list.addItem(item)

    def set_name(self, name):
        self.name_edit.setText(name)

    def set_year(self, year):
        self.year_edit.setValue(year)

    def set_authors(self, authors):
        if not authors:
            return
        for i in authors:
            self.authors_list.findItems(i, Qt.MatchFixedString)[0].setData(Qt.CheckStateRole, Qt.Checked)

    def get_authors(self):
        return [self.authors_list.item(i).text() for i in range(self.authors_list.count())
                if self.authors_list.item(i).checkState() == Qt.Checked]

    def set_genres(self, genres):
        if not genres:
            return
        for i in genres:
            self.genres_list.findItems(i, Qt.MatchFixedString)[0].setData(Qt.CheckStateRole, Qt.Checked)

    def get_genres(self):
        return [self.genres_list.item(i).text() for i in range(self.genres_list.count())
                if self.genres_list.item(i).checkState() == Qt.Checked]

    def get_name(self):
        return self.name_edit.text()

    def get_year(self):
        return self.year_edit.value()

    def ok(self):
        self.presenter.ok()

    def cancel(self):
        self.presenter.cancel()

    def get_presenter(self):
        return self.presenter
