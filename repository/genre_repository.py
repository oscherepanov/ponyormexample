from entites.genre import Genre
from repository.repository_imp import RORepositoryImp


class GenreRepository(RORepositoryImp):
    def __init__(self):
        super().__init__(Genre)

    @staticmethod
    def to_dict(genre):
        return genre.to_dict()