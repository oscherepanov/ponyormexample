from PyQt5.QtWidgets import QHeaderView, QWidget

from presenters.employees_presenter import EmployeesPresenter
from view.q_objects_view import QObjectsView


class QEmployeesView(QObjectsView):
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        QObjectsView.__init__(self, parent)
        self.set_presenter(EmployeesPresenter(view=self))

    def init_table(self):
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(['ФИО', 'Должность'])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)