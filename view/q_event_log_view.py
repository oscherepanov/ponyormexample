from datetime import datetime

from PyQt5.QtWidgets import QLabel, QWidget, QVBoxLayout, QPushButton, QHBoxLayout, QComboBox, QCalendarWidget

from presenters.event_log_presenter import EventLogPresenter


class QEventLogView(QWidget):

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        book_label = QLabel("Книга", self)
        self.book_cmb = QComboBox(self)

        reader_label = QLabel("Читатель", self)
        self.reader_cmb = QComboBox(self)

        employee_label = QLabel("Сотрудник", self)
        self.employee_cmb = QComboBox(self)

        action_label = QLabel("Действие", self)
        self.action_cmb = QComboBox(self)

        created_at_label = QLabel("Дата", self)
        self.created_at_cal = QCalendarWidget(self)

        layout = QVBoxLayout()
        layout.addWidget(book_label)
        layout.addWidget(self.book_cmb)
        layout.addWidget(reader_label)
        layout.addWidget(self.reader_cmb)
        layout.addWidget(employee_label)
        layout.addWidget(self.employee_cmb)
        layout.addWidget(action_label)
        layout.addWidget(self.action_cmb)
        layout.addWidget(created_at_label)
        layout.addWidget(self.created_at_cal)

        self.ok_btn = QPushButton('OK', parent=self)
        self.ok_btn.clicked.connect(self.ok)
        self.ok_cancel = QPushButton('Cancel', parent=self)
        self.ok_cancel.clicked.connect(self.cancel)

        btn_layout = QHBoxLayout()
        btn_layout.addWidget(self.ok_btn)
        btn_layout.addWidget(self.ok_cancel)

        layout.addLayout(btn_layout)
        layout.addStretch(0)

        self.setLayout(layout)

        self.presenter = EventLogPresenter(self)

    def set_book_items(self, items: str):
        self.book_cmb.addItems(items)

    def set_reader_items(self, items: str):
        self.reader_cmb.addItems(items)

    def set_employee_items(self, items: str):
        self.employee_cmb.addItems(items)

    def set_action_items(self, items: str):
        self.action_cmb.addItems(items)

    def set_book(self, name: str):
        self.book_cmb.setCurrentText(name)

    def get_book(self) -> str:
        return self.book_cmb.currentText()

    def set_reader(self, name: str):
        self.reader_cmb.setCurrentText(name)

    def get_reader(self) -> str:
        return self.reader_cmb.currentText()

    def set_employee(self, name: str):
        self.employee_cmb.setCurrentText(name)

    def get_employee(self) -> str:
        return self.employee_cmb.currentText()

    def set_action(self, name: str):
        self.action_cmb.setCurrentText(name)

    def get_action(self):
        return self.action_cmb.currentText()

    def set_created_at(self, value):
        if value:
          self.created_at_cal.setSelectedDate(value)

    def get_created_at(self):
        return datetime.combine(self.created_at_cal.selectedDate().toPyDate(), datetime.min.time())

    def ok(self):
        self.presenter.ok()

    def cancel(self):
        self.presenter.cancel()

    def get_presenter(self):
        return self.presenter
