from presenters.object_presenter import ObjectPresenter
from view.contracts import IPersonView


class PersonPresenter(ObjectPresenter):
    def __init__(self, view: IPersonView, service_class) -> None:
        ObjectPresenter.__init__(self, view, service_class)

    def to_view(self):
        self.view.set_first_name(self.object.first_name)
        self.view.set_last_name(self.object.last_name)
        self.view.set_patronymic(self.object.patronymic)

    def from_view(self):
        self.object.first_name = self.view.get_first_name()
        self.object.last_name = self.view.get_last_name()
        self.object.patronymic = self.view.get_patronymic()
