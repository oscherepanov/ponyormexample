from abc import abstractmethod

from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QVBoxLayout, QPushButton, QHBoxLayout

from view.contracts import IPersonView


class QPersonView(QWidget):

    __meta_class__ = IPersonView

    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.first_name_label = QLabel('Имя', parent=self)
        self.first_name_edit = QLineEdit(parent=self)

        self.last_name_label = QLabel("Фамилия", parent=self)
        self.last_name_edit = QLineEdit(parent=self)

        self.patronymic_label = QLabel("Отчество", parent=self)
        self.patronymic_edit = QLineEdit(parent=self)

        layout = QVBoxLayout()
        layout.addWidget(self.last_name_label)
        layout.addWidget(self.last_name_edit)
        layout.addWidget(self.first_name_label)
        layout.addWidget(self.first_name_edit)
        layout.addWidget(self.patronymic_label)
        layout.addWidget(self.patronymic_edit)

        self.ok_btn = QPushButton('OK',parent=self)
        self.ok_btn.clicked.connect(self.ok)
        self.ok_cancel = QPushButton('Cancel', parent=self)
        self.ok_cancel.clicked.connect(self.cancel)

        btn_layout = QHBoxLayout()
        btn_layout.addWidget(self.ok_btn)
        btn_layout.addWidget(self.ok_cancel)

        layout.addLayout(btn_layout)
        layout.addStretch(0)

        self.setLayout(layout)

    def get_first_name(self):
        return self.first_name_edit.text()

    def set_first_name(self, value):
        self.first_name_edit.setText(value)

    def get_last_name(self):
        return self.last_name_edit.text()

    def set_last_name(self, value):
        self.last_name_edit.setText(value)

    def get_patronymic(self):
        return self.patronymic_edit.text()

    def set_patronymic(self, value):
        self.patronymic_edit.setText(value)

    @abstractmethod
    def ok(self):
        pass

    @abstractmethod
    def cancel(self):
        pass
