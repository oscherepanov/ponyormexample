from datetime import datetime, timedelta

from dto.author_dto import AuthorDTO
from dto.book_dto import BookDTO
from dto.event_log_dto import EventLogDTO
from dto.reader_dto import ReaderDTO
from seed import *

from dto.employee_dto import EmployeeDTO
from dto.position_dto import PositionDTO
from service.action_service import ActionService
from service.author_service import AuthorService
from service.book_service import BookService
from service.employee_service import EmployeeService
from service.event_log_service import EventLogService
from service.genre_service import GenreService
from service.position_service import PositionService

# Create poistion
from service.reader_service import ReaderService

bib_position = PositionDTO(name='Библиотекарь')
bib_position1 = PositionDTO(name='Начальник')

position_service = PositionService()
position_service.create(bib_position)
print(bib_position)
position_service.create(bib_position1)

# Update position
bib_position.name = "Супер библиотекарь"
position_service.update(bib_position)
print(bib_position)

# Get all positions
for i in position_service.get_all():
    print(i)

# Create employee
employee = EmployeeDTO(first_name='Инна', last_name='Петрова', patronymic='Сергеевна', position=bib_position)
employee1 = EmployeeDTO(first_name='Инна1', last_name='Петрова1', patronymic='Сергеевна1', position=bib_position1)

employee_service = EmployeeService()
employee_service.create(employee)
employee_service.create(employee1)

# Load position
employee_service.load_position(employee)
print(employee)

# Update employee
employee.position = bib_position1
employee.first_name = 'Петр'
employee.last_name = 'Петров'
employee.patronymic = None
employee_service.update(employee)
print(employee)

# Get all employees
for i in employee_service.get_all():
    print(i)

# Get genre
genre_service = GenreService()
all_genres = genre_service.get_all()
for i in all_genres:
    print(i)

# Find genre
genre_roman = genre_service.get_by_id(1)

# Create authors
author_service = AuthorService()
tolstoy = AuthorDTO(first_name='Лев1', last_name='Толстой1', patronymic='Николаевич1')
pushkin = AuthorDTO(first_name='Александр', last_name='Пушкин', patronymic='Сергеевич')
author_service.create(tolstoy)
author_service.create(pushkin)

# Get authors
for i in author_service.get_all():
    print(i)

# Update author
tolstoy.first_name = "Лев"
tolstoy.last_name = "Толстой"
tolstoy.patronymic = "Николаевич"
author_service.update(tolstoy)

# Get authors
all_authors = author_service.get_all()
for i in all_authors:
    print(i)

# Create books
book_service = BookService()
war_and_peace = BookDTO(name='Война и мир', authors=all_authors, genres=all_genres, year=1865)
onegin = BookDTO(name='Евгений Онегин', authors=[pushkin], genres=[all_genres[1]], year=1832)
book_service.create(war_and_peace)
book_service.create(onegin)

# Get all books
for i in book_service.get_all():
    book_service.load_genres(i)
    book_service.load_authors(i)
    print(i)

# Update book
war_and_peace.authors = [tolstoy]
war_and_peace.genres = [genre_roman]
book_service.update(war_and_peace)
print(war_and_peace)

# Create reader
reader_service = ReaderService()
reader = ReaderDTO(last_name='Иванов', first_name='Иван', patronymic="Иванович")
reader1 = ReaderDTO(last_name='Петров', first_name='Петр', patronymic="Петрович")

reader_service.create(reader)
reader_service.create(reader1)

# Get readers
for i in reader_service.get_all():
    print(i)

# Update reader
reader.last_name = "Сидоров"
reader.first_name = "Семен"
reader.patronymic = "Сергеевич"
reader_service.update(reader)
print(reader)

# Get all actions
action_service = ActionService()
action_give = action_service.get_by_id(1)
action_ret = action_service.get_by_id(2)
print(action_give)

# Create event log
event_log_service = EventLogService()
event_log_issue = EventLogDTO(book=war_and_peace, reader=reader, employee=employee, action=action_give,
                              created_at=datetime.today())
event_log_return = EventLogDTO(book=war_and_peace, reader=reader, employee=employee, action=action_ret,
                               created_at=datetime.today() + timedelta(days=10))

event_log_service.create(event_log_issue)
event_log_service.create(event_log_return)

print(event_log_issue)

# Update event log
event_log_issue.action = action_ret
event_log_issue.book = onegin
event_log_issue.employee = employee1
event_log_issue.reader = reader1
event_log_issue.created_at += timedelta(days=1)
event_log_service.update(event_log_issue)
print(event_log_issue)

# Get event logs
for i in event_log_service.get_all():
    event_log_service.load_action(i)
    event_log_service.load_book(i)
    event_log_service.load_employee(i)
    event_log_service.load_reader(i)
    print(i)

# Remove event logs
event_log_service.delete(event_log_issue)
event_log_service.delete(event_log_return)

# Remove readers
reader_service.delete(reader)
reader_service.delete(reader1)

# Remove books
book_service.delete(war_and_peace)
book_service.delete(onegin)

# Remove authors
author_service.delete(tolstoy)
author_service.delete(pushkin)

# Remove employees
employee_service.delete(employee)
employee_service.delete(employee1)

# Remove positions
position_service.delete(bib_position1)
position_service.delete(bib_position)