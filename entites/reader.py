from pony.orm import Set

from entites.person import Person


class Reader(Person):
    event_logs = Set('EventLog')