from entites.person import Person
from repository.repository_imp import RORepositoryImp


class PersonRepository(RORepositoryImp):
    def __init__(self):
        super().__init__(Person)