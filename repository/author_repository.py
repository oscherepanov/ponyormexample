from entites.author import Author
from repository.repository_imp import CRUDRepositoryImp


class AuthorRepository(CRUDRepositoryImp):
    def __init__(self):
        super().__init__(Author)

    @staticmethod
    def to_dict(author):
        return author.to_dict(with_collections=True)

    @staticmethod
    def to_dict(author):
        d = author.to_dict()
        del d['classtype']
        return d