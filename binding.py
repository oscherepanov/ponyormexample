from pony.orm import sql_debug
from database import db
from entites import action, author, book, employee, event_log, genre, person, position, reader

sql_debug(True)
db.bind(provider='sqlite', filename='bib.sqlite3', create_db=True)
db.generate_mapping(create_tables=True)
