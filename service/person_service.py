from dto.person_dto import PersonDTO
from repository.person_repository import PersonRepository
from service.service import ROService


class PersonService(ROService):
    def __init__(self):
        super().__init__(PersonDTO, PersonRepository())