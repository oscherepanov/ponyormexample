from database import db, PrimaryKey, Required, Set


class Book(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    authors = Set('Author')
    genres = Set('Genre')
    year = Required(int, min=1500)
    event_logs = Set('EventLog')