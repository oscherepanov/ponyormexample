from abc import ABC, abstractmethod

from dto.dto import DTO


class IMainPresenter(ABC):
    @abstractmethod
    def add_widget(self, widget) -> None:
        pass

    @abstractmethod
    def remove_widget(self, widget) -> None:
        pass

    @abstractmethod
    def show_positions(self) -> None:
        pass

    @abstractmethod
    def show_authors(self) -> None:
        pass

    @abstractmethod
    def show_readers(self) -> None:
        pass

    @abstractmethod
    def show_employees(self) -> None:
        pass

    @abstractmethod
    def show_books(self) -> None:
        pass

    @abstractmethod
    def show_event_logs(self) -> None:
        pass

class ICallBack(ABC):
    @abstractmethod
    def call(self) -> None:
        pass


class IObjectsPresenter(ABC):
    @abstractmethod
    def add(self) -> None:
        pass

    @abstractmethod
    def edit(self) -> None:
        pass

    @abstractmethod
    def delete(self) -> None:
        pass


class IObjectPresenter(ABC):
    @abstractmethod
    def set_object(self, obj: DTO) -> None:
        pass

    @abstractmethod
    def set_callback(self, callback: ICallBack) -> None:
        pass

    @abstractmethod
    def ok(self) -> None:
        pass

    @abstractmethod
    def cancel(self) -> None:
        pass

    @abstractmethod
    def is_ok(self) -> bool:
        pass
