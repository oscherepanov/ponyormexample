from pony.orm import Set
from entites import person


class Author(person.Person):
    books = Set('Book')