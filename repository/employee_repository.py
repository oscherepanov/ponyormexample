from pony.orm import db_session

from entites.employee import Employee
from entites.position import Position
from repository.repository_imp import CRUDRepositoryImp


class EmployeeRepository(CRUDRepositoryImp):
    def __init__(self):
        super().__init__(Employee)

    @staticmethod
    @db_session
    def from_dict(emp):
        args = emp
        args['position'] = Position.get(id=args['position']['id'])
        return args

    @staticmethod
    def to_dict(employee):
        d = employee.to_dict()
        d['position'] = {'id': d['position']}
        del d['classtype']
        return d
